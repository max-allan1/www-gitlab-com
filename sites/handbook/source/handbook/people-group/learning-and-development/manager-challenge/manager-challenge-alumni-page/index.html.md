---
layout: handbook-page-toc
title: Manager Challenge Alumni 
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

## Continuous Learning Opportunities For Manager Challenge Participants

### Manager Challenge Alumni Channel

### Self-paced Learning

Recommended LinkedIn Learning Courses

GitLab Learn - EdCast

### Professional Coaching

As a team member, you have access to multiple avenues for professional coaching to enhance leadership skills. A coach can help a leader identify skills to be developed, key strengths, and strategies for improvement. [Coaching](/handbook/leadership/coaching/) can focus on achieving goals within a leader's current job or move in new direction. 

GitLab offers the following benefits for team members to receive coaching: 

1. [Modern Health Coach](https://about.gitlab.com/handbook/total-rewards/benefits/modern-health/): Through GitLab, you have access to 5 coaching sessions and 6 therapy sessions at no cost to you. It's an easy to use resource for people leaders interested in exploring coaching for the first time, or for leaders who want to be reacuainted with a coach. 

2. [Growth & Development Budget](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development/): Team members have access to up $10,000 USD to use for professional development, including coaching. Managers can find coaches and use the funds for personalized leadership coaching that is action-oriented for professional development. 

### LifeLabs Learning

GitLab L&D is exploring additional management training with [LifeLabs Learning](https://lifelabslearning.com/). More details to come. 

### Leadership Chats

Join the [Leadership Chats](/handbook/people-group/learning-and-development/manager-challenge/leadership-chats/) which are monthly calls for people leaders at GitLab. These forums are a great way for managers to continue the discussion following the Manager Challenge program. 

## Aspiring Managers Resources

- Slack Channel

### Self Paced Learning 

### Coffee Chat with Manager Challenge Alumni

If you are interested in participating in the [Manager Challenge program](/handbook/people-group/learning-and-development/manager-challenge/), feel free to schedule a coffee chat with any of the Manager Challenge alumni. You can review the list of alumni below. The chats are helpful to gain a better understanding of the program, what to expect, and how to participate. 

## List of past Manager Challenge Participants
