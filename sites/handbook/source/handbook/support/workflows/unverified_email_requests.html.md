---
layout: handbook-page-toc
title: Unverified Email Requests
category: GitLab.com
subcategory: Accounts
description: Process for removing an unverified email from a user account for GitLab.com.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

Use this workflow when a user cannot add an email address to their account because it is on a different account _and_ is unverified.

## Workflow

1. Confirm that the email address is added onto a different account and is unverified.
1. Create a ticket for that user and inform them that another user is trying to add the email address. Give them the opportunity to verify it within a week.
1. If unverified or no response within a week, remove the unverified email address from the account. If the email address is their primary and only email address, inform the original requestor to wait for the 90 day auto-deletion period.
