---
layout: handbook-page-toc
title: "Community Relations Program Management"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The Community Relations team works from issues and [issue boards](#community-relations-issue-boards). If you need our assistance with any project, please open an issue and use one of the [_main program labels_](#community-relations-labels) anywhere within the GitLab repo.

## Community Relations handbook updates

Merged updates to the handbook are posted to the [#community-relations-fyi](https://gitlab.slack.com/archives/C015YDXTREK) Slack channel.

MRs must be tagged with the `Community Relations` labeled *before being merged* in order to be posted to Slack. When you create a new MR, be sure to add the label upon creation so this step isn't forgotten. If you are assigned to merge an MR, ensure the label is added prior to merging.

## Community Relations issue boards

| Board | Program | Usage | Notes |
| --- | --- | --- | --- |
| [OKRs](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics?label_name[]=OKR) | _Team_ | Quarterly OKR status tracking |  |
| [Community Team Content](https://gitlab.com/groups/gitlab-com/-/boards/1591627?label_name[]=Community%20Team%20Content) | _Team_ | Blog content the team is working on | Top level group board |
| [Community Team, by assignee](https://gitlab.com/groups/gitlab-com/-/boards/1591691) | _Team_ | Issue workload per team member | Top level group board |
| [Evangelist Program](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/boards/951386?label_name[]=Evangelist%20Program) | Evangelists | Work related to the Evangelist Program |  |
| [Meetups](https://gitlab.com/groups/gitlab-com/marketing/-/boards/962542?&label_name[]=Meetups) | Evangelists | Meetup tracking and organization |  |
| [Code Contributor Program](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/boards/1034162?&label_name[]=Code%20contributor%20program) | Code contributors | Work related to the Code Contributor Program |  |
| [Education Program](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/boards/952267?&label_name[]=education) | Education | Work related to the Education Program |  |
| [Open Source Program](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/boards/944792?&label_name[]=Open%20Source) | Open Source | Work related to the Open Source Program |  |
| [Open Source Program: migrations](https://gitlab.com/groups/gitlab-com/-/boards/1622882?&label_name[]=Open%20Source%20Program&label_name[]=movingtogitlab) | Open Source | Plan, execute, track Open Source migrations and partnerships | Issues on this board are generally private |
| [Community Advocacy](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/boards/781438?label_name[]=Community%20Advocacy) | Advocates | Work related to the Community Advocates team |  |
| [Developer Evangslism](https://gitlab.com/groups/gitlab-com/-/boards/1565342?label_name[]=dev-evangelism) | Developer Evangelism | Developer Evangelism issues across the entire `gitlab-com` group |  |


## Community Relations labels

- We track the workflow with the [Marketing Status labels](/handbook/marketing/#boards-and-labels).
- Some programs use [scoped labels in addition](/handbook/marketing/#functional-team-labels).
- Main program labels are defined either at the top `gitlab-com` or the `gitlab-com/marketing` group level to facilitate cross-collaboration and visibility across issue boards

| Label | Program | Usage | Notes |
| --- | --- | --- | --- |
| `Community Relations` | Community Relations | **Main team label** | `gitlab-com` group label. Merge requests with this label generate automatic notifications for the Community team via the Slack channel #community-relations when they are merged |
| `Open Source Program` | Open Source | **Main program label** | `gitlab-com` group label |
| `Open Source` | Open Source |  | `gitlab-org` group label |
| `OSS Stage::Evaluation` | Open Source | Open source migration status: evaluating |  |
| `OSS Stage::In Progress` | Open Source | Open source migration status: WIP |  |
| `OSS Stage::Blocked` | Open Source | Open source migration status: blocked |  |
| `OSS Stage::Review` | Open Source | Open source migration status: review |  |
| `movingtogitlab` | Open Source | Track open source migrations |  |
| `Evangelist Program` | Evangelists |  **Main program label**: Tech meetups, tech speakers, and community-driven evangelism |  |
| `Meetups` | Evangelists | GitLab Meetups planning and tracking |  |
| `Heroes` | Evangelists |  Heroes applications, content, speaking engagements, and other Heroes program issues  |  |
| `Speaker Needed` | Evangelists |  Events or meetups that need a speaker |  |
| `become-a-speaker` | Evangelists |  Support for community members who want to become tech speakers  | Community Relations group label |
| `heroes-content` | Evangelists |  For issues related to a blog, video, or other content generated by a GitLab Hero  | Project label |
| `heroes-talk` | Evangelists |  For issues related to a CFP submission or tech talk given by a GitLab Hero  | Project label |
| `request-for-heroes` | Evangelists | Share speaking, blogging, and other contribution opportunities with GitLab Heroes  | Project label |
| `Code contributor program` | Code contributors |  **Main program label**: for general issues related to the program  |  |
| `1st contribution` | Code contributors | [Automatic contribution label](/handbook/marketing/community-relations/code-contributor-program/#contribution-labels) | `gitlab-com` group label |
| `1st contribution` | Code contributors | [Automatic contribution label](/handbook/marketing/community-relations/code-contributor-program/#contribution-labels) | `gitlab-org` group label |
| `Community contribution` | Code contributors | [Automatic contribution label](/handbook/marketing/community-relations/code-contributor-program/#contribution-labels) | `gitlab-com` group label |
| `Community contribution` | Code contributors | [Automatic contribution label](/handbook/marketing/community-relations/code-contributor-program/#contribution-labels) | `gitlab-org` group label |
| `Hackathon` | Code contributors | Community contribution during GitLab Hackathon events | `gitlab-org` group label |
| `GitLab Hackathon` | Code contributors | Community contribution during GitLab Hackathon events | `gitlab-com` group label |
| `OKRs` | _Team_ | Quarterly team OKRs |  |
| `Community Team Content` | _Team_ | Track the status of content created by the team. Generally blog posts | Top-level group label |
| `education program` | Education | **Main program label**: for general issues related to the program |  |
| `Community Advocacy` | Advocates |  **Main program label**: for general issues related to the program |  |
| `Merch` | Advocates | Issues related to merchandise handling and merchandise requests |  |
| `dev-evangelism` | Developer Evangelism | Any issue in the `gitlab-com` group where the developer evangelism team is involved |  |

## Community Relations Group Conversation
### What is a Group Conversation (GC)?
GitLab teams update the rest of the company on what they are achieving and working on through [Group Conversations](/handbook/group-conversations/). Group Conversations are an important way to make sure the rest of the organization is aware of what each team is up to, has an opportunity to ask questions, and have a collection of links and information for reference.

### Old Group Conversation decks
The Community Relations team's previous decks are in our [Google drive](https://drive.google.com/drive/folders/11c011VkzvSIcS3ko98AbvEP4UcZJmfBk?usp=sharing) (GitLab employees only). It's useful when putting together the next GC deck to look at the last one to make sure that items on the calendar edge don't get missed or included a second time.

### Group Conversation Directly Responsible Individual (DRI)
A DRI from the Community Relations team will be identified for each GC approximately 3 weeks before the next scheduled GC. The DRI should rotate through team members to give everyone a chance to lead the GC. The DRI is responsible for:

- Creating an issue in the [Community Building Project](https://gitlab.com/gitlab-com/marketing/community-relations/general) using the [community-relations-group-conversation template](https://gitlab.com/gitlab-com/marketing/community-relations/general/-/issues/new?issuable_template=community-relations-group-conversation).
-  Complete all the tasks in the group conversation template.
-  Prepare a few questions for the team prior to the GC in the case there aren't any questions in the agenda during the GC.
-  Lead the GC.
-  Close the GC issue.
