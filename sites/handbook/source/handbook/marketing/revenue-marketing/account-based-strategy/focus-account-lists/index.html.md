---
layout: handbook-page-toc
title: "Focus Account Lists"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Focus Accounts
We used have a Focus Account Lists (FAL) for both our Large and Mid-Market segments.  These lists accounted for our `Account Centric` or 1:many marketing motion and were updated quarterly.  Because these lists were static, we have itereated to, rather than having a static list of focus accounts we now have certain account lists we focus on.  This allows us to focus on an account at the right time in the buyer journey with the right marketing motion.

### Large Focus Accounts (formerly known as the GL4300)
1. ICP accounts- any account that matches our ICP
1. `first order available` accounts that have shown engagement.  This is all `first order` accounts that are `engaged w no people` and below in our marketing journey funnel.  Journey stages are outlined here for reference.
1. `new logo target account` accounts that are identified in SFDC.  These accounts are a subset of our `first order` total addressable market that are eligible for the `new logo` additional compensation for the Sales Organization.
1. Sales nominated- sales flags priority accounts in SFDC using the `account rank` field. Instructions to do this for your accounts is in SFDC are [here](/handbook/sales/field-operations/gtm-resources/).  **the exception to this rule is Public Sector because of how these account hierarchies are set up.  For PubSec, we include ranked accounts but do not apply the `first order available` requirement for these accounts.

### Mid Market Key Accounts (formerly known as the MM4000)
1. `first order` logos that are `engaged w no people` and below in our marketing journey funnel. Journey stages are outlined here for reference.



### How do I identify a `focus account` or what strategy an account is in?
#### Accounts are identified in Salesforce by the GTM strategy field in Salesforce:

**Volume**
Default selection for all accounts that are not currently in an `account centric` or `abm` motion.

**TOTAL ADDRESSABLE MARKET**
All `first order available` accounts that are not in our account centric motion

**ACCOUNT CENTRIC**
1:many marketing strategy.  Sometime referred to as programmatic ABM.

**ABM**
All accounts that are currently in our ABM programs (1:1 and 1:few).


## Account hierarchy
**If a subsidiary of a conglomerate is a customer, is the parent (and all other subsidiaries) considered a customer (i.e. not net new logo)?**
Yes, as our 2H plan is focused on NEW FIRST ORDER therefore if a child account is a customer, then an account would not be included. These would be considered Connected New.
## Connected New Customer / Net New Logo
A connected new customer (sometimes called net new logo) is the first new subscription order with an Account that is related to an existing customer Account Family (regardless of relative position in corporate hierarchy) and the iACV related to this new customer is considered "Connected New".

There is a field called `Order Type` in Salesforce that on the back end automatically captures New - First Order, Connected New, and Growth. Marketing is currently focused on increasing New - First Order.

### Data sources
We use a variety of data sources to determine if an account matches one of our ideal customer profile data points.  The table below shows the order of operation of where we look for this data.

| Attribute | Data Sources (in order of priority) |
| ------ | ------ |
| Number of developers | Aberdeen number of developers --> user/SAL input in Salesforce --> No. of employees as a proxy |
| Technology stack | Gainsight input --> Aberdeen technology stack --> user/SAL input in Salesforce --> Zoominfo tech stack  |
| Cloud provider | Aberdeen technology stack --> user/SAL input in Salesforce --> Zoominfo tech stack |
| Prospect | Total CARR for all accounts within the hierarchy equals zero |

### When an account moves to/from our GL4300 & MM4000 focus account lists
Both Focus lists will be reviewed and modified during the last 2 weeks of each quarter.  The process is as follows:
##### T-minus two weeks from end of quarter
1. ABM nominations are made by sales

##### Last day of quarter
1. Closed won logos will be filtered out and the lists will be refilled based on next up ideal customer profile accounts. 
1. Accounts not showing an increase or are showing a decrease in engagement will be removed from the Focus list
1. Any additional accounts identified by sales as not a good prospect due to lack of budget, etc

##### First business day of new quarter
1. Salesforce is updated with all changes to the focus acccount list and shared with the organization




## Account Sources
All accounts in Salesforce that are part of the `ICP Total Addressable Market` will also have the `New Logo Target Account` field completed.

**Existing** An account that already existed in our circumstances

**Core** Newly identified core user

**Lookalike** Account identified based on attributes that match our exisitng customer base

### Aberdeen Data
As part of the development of our ideal customer profile, we purchased data from [Aberdeen](https://www.aberdeen.com/?gclid=Cj0KCQiAqdP9BRDVARIsAGSZ8AlzfX6vYnVNh7YX2IKrc6uNhqjfGY6sQywcyZalJScxTyexilB0pa4aAvFdEALw_wcB) to help us determine our ICP total addressable market.  The data included number of developers, specific technologies installed, and cloud provider.  The data is rolls up to the `Ultimate Parent` as we are looking for both the best entry point for an account and the overall environment.

| Data point | Salesforce field | Description & how to use the data |
| ------ | ------ | ------ |
| Number of developers | `Aberdeen Ultimate Parent Developer Count`  | This number is the total number of current developer contacts that Aberdeen has in their database for all sites of a company.  Because it is impossible to have a database of ALL contacts at a company, we look to this data point to verify if an account has over 500 developers IF the account has a number >500 in this field but we don't exclude an account from our TAM if thecount is lower than 500 due to the nature of the data point, rather, we go to our next best data point to verify. |
| Competitive technology | `Aberdeen Ultimate Parent Technology Stack` | This field identifies if a company has a certain technology in their technology stack that is part of our ideal customer profile |
| Cloud provider | `Aberdeen Ultimate Parent Cloud Provider` | Tells us if an account has AWS, GCP, or both as their cloud provider. |

**What does the Demandbase intent score mean?**
[Demandbase handbook page](/handbook/marketing/revenue-marketing/account-based-strategy/demandbase/)
You can read all about Demandbase and how they score accounts (both numerically and by H/M/L) in the handbook page dedicated to Demandbase.

**As we run our campaigns against this list and learn more about which accounts are engaging/not, is there a process to take this learning into account for removing/adding accounts to the list?**
The GL4300 and MM4000 are both dynamic audiences. When an account is closed won/disqualified, it will be dropped from the list and refilled 1x a quarter ahead of sales QBR's.

**For accounts outside the list that are engaging, how can we share that information so they can get added to the list?**
We are using Demandbase to look track all accounts that fit our ideal customer profile.  We can see an increase in engagement in the platform and will be adding to the list quarterly.

**At a high level, how does the intent data get collected?**
[Handbook page](/handbook/marketing/revenue-marketing/account-based-strategy/demandbase/#intent-fields--definitions) 

## Where can I see the GL4300 and MM4000 account lists?
Both the GL4300 and MM4000 are identified in Salesforce by the `GTM Strategy` field = `Account Centric`. From the you can filter by segment to see the respective lists, or by account owner etc to filter further.

## How do I surface an account for review to be added to the `Focus Account Lists`?
Because we do not have all of the data possible in Salesforce, some accounts are not surfaced through our process for refilling the FAL's quarterly.  If you have an account that should be reviewed, please ensure the following is complete in Salesforce, then chatter @emilyluehrs to flag the account for review.
- number of developers (`potential users verify` field)
- technology stack is up to date (please use the Gainsight `stage technology` fields to do this)
- account hierarchy is up to date
