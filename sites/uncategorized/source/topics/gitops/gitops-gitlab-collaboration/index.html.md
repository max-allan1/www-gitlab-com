---
layout: markdown_page
title: "Why GitLab’s collaboration technology is critical for GitOps: A demo"
description: "Collaboration software, like GitLab, makes GitOps workflows easier."
---

[GitOps](/topics/gitops/) refers to using a Git repository as the single source of truth for all the code that goes into building infrastructure and deploying applications. By using a version control system, such as Git, as the single source of truth, engineers are able to update the underlying source code for their applications in a continuous delivery format. 

The [version control](/topics/version-control/) system ensures documentation and visibility, while an audit trail enables compliance. GitOps makes it easy to revert changes and provides one place to access the most current information to help teams understand the current state from the perspective of both software development and operations teams.

# GitOps and GitLab

GitLab is a single application for the entire DevOps lifecycle and serves as a [collaboration](/blog/2020/11/23/collaboration-communication-best-practices/) platform that empowers stakeholders to weigh in on the code production process. Collaboration is an important aspect of the GitOps process, because teams across the entire development lifecycle - from infrastructure and development teams to security and business stakeholders - require a seamless method to collaborate to ship code quickly and efficiently. 

> [GitOps isn't just about the code](/solutions/gitops/), it’s about the collaboration, and GitLab enables every team to work in a single platform. 

# Using GitLab collaboration features for GitOps

_The remaining article includes a demo on how GitLab powers GitOps through collaboration. The demo covers example epics and issues, which are linked in subsequent sections._ 

## Planning a project with epics

Since GitOps is deployment centered on version control, the first step is to define the scope of the project and identify the stakeholders. Next, team members can share any other information that might be necessary to make the project happen, such as coding, changes to infrastructure as code, what changes must be reviewed, and eventually deployed to production.

After opening an [epic](/blog/2020/01/21/epics-three-features-accelerate-your-workflow/) in the associated repository, teams can add goals and tasks in the description. An epic enables teams to track issues across different [projects](/blog/2020/04/02/security-trends-in-gitlab-hosted-projects/) and milestones. An [issue](/blog/2018/08/02/4-ways-to-use-gitlab-issue-boards/) is the main medium for collaborating ideas and planning work in GitLab. 

**Example epic and issues**

> In this example epic, called [Scale the Cloud](https://gitlab.com/groups/gitops-demo/infra/-/epics/2), teams can view the process behind scaling up a Kubernetes cluster in GitLab. Because GitLab is [multicloud](/blog/2020/06/30/many-meanings-multicloud/), there are three separate issues for the demo that articulate what is required to deploy the [Kubernetes](/blog/2019/10/24/kubernetes-101/) cluster to each unique environment: [Azure (AKS)](https://gitlab.com/gitops-demo/infra/azure/issues/1), [Google (GKE)](https://gitlab.com/gitops-demo/infra/gcp/issues/4), and [Amazon (EKS)](https://gitlab.com/gitops-demo/infra/aws/issues/3).


## Fostering collaboration and transparency with GitLab

At the epic level, teams can see that the issue for scaling inside the EKS cluster has already been completed. Clicking the issue reveals that a merge request was created from the tasks outlined in the issue, and that the MR is already [merged](/blog/2020/01/30/all-aboard-merge-trains/).

To see what exactly has changed between the original code and current code, click inside the MR. From here, teams can see that all the tests that passed before/after merging, consult the comment history to identify changes, and make a note who approved and merged the code. 

The issue for scaling to [GKE](/blog/2020/03/27/gitlab-ci-on-google-kubernetes-engine/) is not yet completed. The merge request is still a [Work in Progress (WIP)](https://docs.gitlab.com/ee/user/project/merge_requests/work_in_progress_merge_requests.html#work-in-progress-merge-requests), meaning nothing has been changed yet. There is a comment on the MR from Terraform, which shows that the node count needs to change from two nodes to five nodes to prepare the GKE environment for deployment. Whoever is the approver for the MR clicks `Resolve the WIP Status` to kick off the pipeline and can opt to delete the source branch to merge the updated node count.

In order for GitLab to be an effective collaboration tool, it also needs to be transparent which is why everyone in the organization is able to see an issue and associated MR by default. The issue and MR can be assigned to a collaborator, or the collaborator can be tagged in the comments section to have it added to their [To Do list](https://docs.gitlab.com/ee/user/todos.html).

Navigating back to the Epic view, which is what stakeholders will often use to view project progress, teams can see that the deployment for scaling GKE to five nodes is underway.

Using GitLab for a GitOps <a href="https://thenewstack.io/what-is-gitops-and-why-it-might-be-the-next-big-thing-for-devops/" target="_blank">workflow</a>, every team member is able to work from the same system and understand the status of projects. Whether in infrastructure or in application development, all changes follow the same process of, defining the body of work, assigning it to individuals, collaborating with teammates, and deploying that code and using the Git repository as that single source of truth.

## Demo: How GitLab empowers GitOps 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube-nocookie.com/embed/wk7YAXijIZI" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->
 
# Ready to learn more about GitOps?
 
* [Discover how GitLab streamlines GitOps workflos](/solutions/gitops/)
* [Learn about the future of GitOps from tech leaders](/why/gitops-infrastructure-automation/)
* [Download the beginner’s guide to GitOps](https://page.gitlab.com/resources-ebook-beginner-guide-gitops.html)
